import { Master } from "./index";

export type Alert = {
    id?: string | number,
    master: Master,
    product: string,
    date: string,
    alertType: string,
    filename: string,
    status: string,
    inputAlertReadNumber: number,
    inputAlertRejectedNumber: number,
    inputAlertProcessedNumber: number,
}