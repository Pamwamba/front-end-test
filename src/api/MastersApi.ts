import { Master } from "../types/index";
import { URLS } from "../utils/AppConstants";

const BASE_URL: string = (process.env.REACT_APP_BASE_URL as string);

export async function listMasters(): Promise<Master[] | undefined> {
  return fetch(
    `${BASE_URL}${URLS.LIST_MASTERS}`,
    {
      method: "GET",
    }
  )
    .then((r) => r.json())
    .catch((error) => {
      console.error(error);
      return {};
    });
}