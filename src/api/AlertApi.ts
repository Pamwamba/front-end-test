import { Alert, Master } from "../types/index";
import { URLS } from "../utils/AppConstants";

const BASE_URL: string = (process.env.REACT_APP_BASE_URL as string);

let mockedAlerts: Alert[] = [];

const status = ['SUCCESSFUL', 'PENDING', 'FAILED']

// Create mocked data
for (let i = 0; i < 50; i+=1) {
  const alert = {
    master: {
      id: i,
      legalId: (Math.random() * (9999999 - 1000000) + 1000000).toString(),
      businessName: `INTRACTIV ${i}`
    },
    product: "CREDITSAFE_CONNECT",
    date: "2022-01-27",
    alertType: "",
    filename: "",
    status: status[Math.floor(Math.random() * 3)],
    inputAlertReadNumber: Math.floor(Math.random() * 15),
    inputAlertRejectedNumber: Math.floor(Math.random() * 15),
    inputAlertProcessedNumber: Math.floor(Math.random() * 15)
  }
  mockedAlerts.push(alert);
}

export async function alertAudits(master: Master, minDateFilter: string, maxDateFilter: string): Promise<Alert[]> {
  return mockedAlerts;
  return fetch(
    `${BASE_URL}${URLS.ALERT_AUDITS}`,
    {
      method: "POST",
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify({master, minDateFilter, maxDateFilter}),
    }
  )
    .then((r) => r.json())
    .catch((error) => {
      console.error(error);
      return undefined;
    });
}