export const TITLES = {
  ALERT_TITLE: 'Alertes',
  HEADER_TITLE: 'Intractiv BAM'
}
export const URLS = {
  ALERT_AUDITS: '/alert-audits',
  LIST_MASTERS: '/list-masters'
}
