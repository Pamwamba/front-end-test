import { DataGrid, GridColDef } from '@mui/x-data-grid';
import { Alert } from '../types';
import { TITLES } from '../utils/AppConstants';
import './alertDataTable.css';

type Props = {
  alerts: Alert[];
}

const AlertDataTable = ({alerts}: Props) => {
  // Remove 'master' field and add an id as required by the doc of @mui/x-data-grid
  const rows = alerts ? alerts.map(({master, ...keepAttrs}) => {
    keepAttrs.id = master.id;
    return keepAttrs
  }) : []

  const columns: GridColDef[] = [
    {
      field: 'product',
      headerName: 'Produit',
      minWidth: 200,
      editable: false,
      sortable: false,
      disableColumnMenu: true,
      flex: 1,
    },
    {
      field: 'date',
      headerName: 'Date',
      minWidth: 150,
      editable: false,
      sortable: false,
      disableColumnMenu: true,
      flex: 1,
    },
    {
      field: 'alertType',
      headerName: 'Type',
      type: 'number',
      width: 110,
      editable: false,
      sortable: false,
      disableColumnMenu: true,
    },
    {
      field: 'filename',
      headerName: 'Nom du fichier',
      width: 160,
      editable: false,
      sortable: false,
      disableColumnMenu: true,
    },
    {
      field: 'status',
      headerName: 'Statut',
      type: 'number',
      width: 110,
      editable: false,
      sortable: false,
      disableColumnMenu: true,
    },
    {
      field: 'inputAlertReadNumber',
      headerName: 'Lues',
      type: 'number',
      width: 100,
      editable: false,
      sortable: false,
      disableColumnMenu: true,
    },
    {
      field: 'inputAlertRejectedNumber',
      headerName: 'Rejetées',
      type: 'number',
      width: 100,
      editable: false,
      sortable: false,
      disableColumnMenu: true,
    },
    {
      field: 'inputAlertProcessedNumber',
      headerName: 'Traitées',
      type: 'number',
      width: 100,
      editable: false,
      sortable: false,
      disableColumnMenu: true,
    },
  ];

  return (
    <div className='content'>
      <div className='alertTitleContainer'>
        <div className='alertTitle'>{TITLES.ALERT_TITLE}</div>
      </div>
      <DataGrid
        autoHeight
        rows={rows}
        columns={columns}
        pageSize={20}
        rowsPerPageOptions={[20]}
        disableSelectionOnClick
        disableExtendRowFullWidth
      />
    </div>
  )
}

export default AlertDataTable;