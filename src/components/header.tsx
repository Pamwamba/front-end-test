import { AppBar, Typography } from "@mui/material";
import { Box } from "@mui/system";
import { TITLES } from "../utils/AppConstants";

const Header = () => {
  return (
    <Box sx={{ flexGrow: 1, mb: 2}}>
    <AppBar position="static">
      <Typography variant="h6" component="div" sx={{ flexGrow: 1, padding: 2 }}>
        {TITLES.HEADER_TITLE}
      </Typography>
    </AppBar>
  </Box>
  )
}

export default Header;