import { FormControl, InputLabel, MenuItem, Select } from "@mui/material";
import { useState } from "react";
import { Master } from "../types";

type Props = {
  label: string;
  menuValues?: Master[];
  setMaster: Function;
}

const SelectMasters = ({label, menuValues, setMaster}: Props) => {
  const [value, setValue] = useState('');

  const getMasterByLegalId = (legalId: string) => {
    return menuValues?.find(master => master.legalId === legalId);
  }

  const handleChange = (event: any) => {
    setValue(event.target.value);
    const currentMaster = getMasterByLegalId(event.target.value);
    setMaster(currentMaster);
  };

  return (
    <FormControl sx={{ m: 1, width: 300 }}>
      <InputLabel id="demo-simple-select-label">{label}</InputLabel>
      <Select
        labelId="demo-simple-select-label"
        id="demo-simple-select"
        value={value}
        label={label}
        onChange={handleChange}
      >
       {menuValues &&
         menuValues!.map((value: Master) => (
          <MenuItem key={value.id} value={value.legalId}>{`${value.businessName} (${value.legalId})`}</MenuItem>
         ))
       }
      </Select>
    </FormControl>
  )
}

export default SelectMasters;