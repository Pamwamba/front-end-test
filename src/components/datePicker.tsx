import MobileDatePicker from '@mui/lab/MobileDatePicker';
import { FormControl, TextField } from '@mui/material';
import { useState } from 'react';

type Props = {
  label: string,
  setDate: Function,
  min?: Date,
  max?: Date,
}

const DatePicker = ({label, setDate, min, max}: Props) => {
  const [value, setValue] = useState<Date | null>(null);
  return (
    <FormControl sx={{ m: 1, width: 300 }}>
      <MobileDatePicker
        label={label}
        value={value}
        onChange={(newValue) => {
          setValue(newValue);
        }}
        onAccept={() => setDate(value)}
        renderInput={(params) => <TextField {...params} />}
        minDate={min}
        maxDate={max}
        />
    </FormControl>
  )
}

export default DatePicker;