import { Stack } from "@mui/material";
import { formatISO } from "date-fns";
import { useEffect, useState } from "react";
import { alertAudits } from "../api/AlertApi";
import { listMasters } from "../api/MastersApi";
import { Alert, Master } from "../types";
import AlertDataTable from "./alertDataTable";
import DatePicker from "./datePicker";
import SelectMasters from "./selectMasters";

const Form = () => {
  const [masterMenu, setMasterMenu] = useState<Master[] | undefined>([]);
  const [alertData, setAlertData] = useState<Alert[]>([]);
  const [master, setMaster] = useState<Master | null>(null);
  const [minDate, setMinDate] = useState<Date | undefined>(undefined);
  const [maxDate, setMaxDate] = useState<Date | undefined>(undefined);

  // Convert the date to a string "YYYY-MM-DD".
  const formatDate = (date: Date | undefined) => {
    return date ? formatISO(date, { representation: 'date' }) : "";
  }

  const postAlert = () => {
    if (master) {
      alertAudits(master, formatDate(minDate), formatDate(maxDate))
        .then((results) => setAlertData(results))
        .catch(() => setAlertData([]))
    }
  }

  // Perform the post request to /alert-audits when one of the 3 fields is updated.
  useEffect(() => {
    postAlert();
  }, [master, minDate, maxDate]);

  // Get the list of masters when the component loads.
  useEffect(() => {
    listMasters()
      .then((results) => setMasterMenu(results))
  }, [])

  return (
    <>
      <Stack direction="row" spacing={2} justifyContent={'center'} alignItems={'center'}>
          <SelectMasters label="Sélectionner un master" menuValues={masterMenu} setMaster={(master: Master) => setMaster(master)}/>
          <DatePicker label="Sélectionner une date de début" setDate={(date: Date) => setMinDate(date)} max={maxDate}/>
          <DatePicker label="Sélectionner une date de fin" setDate={(date: Date) => setMaxDate(date)} min={minDate}/>
      </Stack>
      <AlertDataTable alerts={alertData}/>
    </>
  )
}

export default Form;