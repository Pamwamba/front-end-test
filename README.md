# Getting Started

This project was developed with node v16.13.1, make sure you are running with the correct version.

Create .env at the root of your project and add the following variables :

- REACT_APP_BASE_URL=[BASE_URL]

Run `npm install`, then `npm start`.

# Alert audits

You can find mocked data in AlertApi.ts, if you want to use the api, just remove line 31.

# Todo

- Add unit tests.
- Change Datagrid to Tables in order to display the subtotal of alerts.
